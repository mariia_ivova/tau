public class Address {
    String country;
    String city;
    String street;
    int homenumber;
    int flat;
    int index;

    public Address(String country, String city, String street, int homenumber, int flat, int index) {
        this.country= country;
        this.city = city;
        this.street = street;
        this.homenumber = homenumber;
        this.flat = flat;
        this.index = index;
    }

    public Address(String country, String city, String street){
        this.country = country;
        this.city = city;
        this.street = street;
    }

}
