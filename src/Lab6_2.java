import java.util.Arrays;

public class Lab6_2 {
    public static void main(String[] args){
        // Выполнить реверсирование массива (первый элемент становится последним, последний - первым и т.д).
        // Вывести элементы первоначального массива и после выполнения реверсирования.
        int[] myArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        int[] reverse = new int[myArray.length];
        int size = myArray.length;
        for (int i = 0; i < size / 2; i++) {
            int temp = myArray[i];
            reverse[i] = myArray[size - 1 - i];
            reverse[size - 1 - i] = temp;
        }
        System.out.println("Array after reverse:: " + Arrays.toString(reverse));
        System.out.println("Array as is " + Arrays.toString(myArray));
    }
}
