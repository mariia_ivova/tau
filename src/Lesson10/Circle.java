package Lesson10;

public class Circle extends Figures {
    int diameter;

    public Circle(int diameter) {
        this.diameter = diameter;
        this.area = (Math.PI * Math.pow((diameter/2), 2))/2;
        this.perimeter=Math.PI*diameter;

    }

}
