package Lesson10;

import java.awt.*;

public class Main {
    public static void main (String [] args){
        Point one=new Point(5,5);
        Rectangle prjamougolnik = new Rectangle(9,10,one);
        System.out.println(prjamougolnik.getArea());
        System.out.println(prjamougolnik.getPerimeter());
        Point two = new Point(5,10);
        prjamougolnik.movePoint(two);

        Circle krug = new Circle(10);
        System.out.println(krug.getArea());
        System.out.println(krug.getPerimeter());
        System.out.println();

        Square kvadrat = new Square(6,one);
        System.out.println(kvadrat.getArea());
        System.out.println(kvadrat.getPerimeter());

    }
}
