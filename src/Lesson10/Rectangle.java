package Lesson10;

import java.awt.*;

public class Rectangle extends Figures {
    int height;
    int width;
    Point topLeftCorner;

    public Rectangle (int height, int width, Point topLeftCorner){
        this.height = height;
        this.width = width;
        this.topLeftCorner = topLeftCorner;
        this.perimeter = (2*height+2*width);
        this.area=(height*width);
    }

    public void movePoint(Point point){
        topLeftCorner=point;
    }

}
