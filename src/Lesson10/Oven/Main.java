package Lesson10.Oven;

public class Main {
    public static void main (String[] args){
        GasOven one = new GasOven();
        one.turnOn();
        one.changeTemperature(200);
        System.out.println(one.type+" "+one.turnOn+" "+one.temperature);

        ElectricOven two = new ElectricOven();
        two.turnOn();
        two.changeTemperature(100);
        System.out.println(two.type +" "+two.turnOn+" "+two.temperature);
    }
}
