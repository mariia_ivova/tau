package Lesson10.Oven;

public class Oven {
    boolean turnOn = false;
    int temperature = 0;
    boolean isUsed;
    String type;


    public void changeTemperature (int temperature){
        if (turnOn){
        this.temperature = temperature;
        } else {
            System.out.println("It is turned off");
        }
    }

    public void turnOn(){
        if (turnOn){
            System.out.println("It is already turn On");
        } else {
            turnOn = true;
        }

    };

    public void turnOff (){
        if (!turnOn){
            System.out.println("It is turn off");
        } else {
            turnOn = false;
            temperature = 0;
        }
    }


}
