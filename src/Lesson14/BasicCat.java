package Lesson14;
/*
1)Создать класс BasicCat (с полями name, age, weight).

Добавить метод (НЕ в классе BasicCat), принимающий на вход экземпляр класса Cat и печатающий возраст кошки,
приведенный к условному “человеческому”. Для получения этого значения нужно умножить возраст кошки на 7
(коэффициент на который умножаем должен быть отдельной константой). Вызвать этот метод.

2)Определите в классе BasicCat статическую переменную numKittens, и добавьте логику в конструктор, чтобы эта переменная
увеличивалась, когда создается экземпляр класса с возрастом, меньшим 1 года. Добавьте метод, печатающий количество
созданных котят за всю историю существования класса.

3)Добавьте в класс BasicCat массив, который бы хранил ссылки на все когда-либо созданные экземпляры в статической
переменной. Хранить переменные в массиве, корректно обрабатывать ситуацию переполнения массива создавать новый массив,
 не терять то, что было. Добавьте метод, который выводит на экран количество созданных экземпляров.
*/
public class BasicCat {
    String name;
    double age, weight;
    private static int numKittens;
    static BasicCat[] catsList = new BasicCat[1];
    static private BasicCat[] catsReserve;

    BasicCat(String name, double age){
        this.name = name;
        this.age = age;
        if (age < 1){
            numKittens++;
        }
        addCat(this);
    }

    BasicCat(String name, double age, double weight) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        if (age < 1) {
            numKittens++;
        }
        addCat(this);
    }

    private static void addCat(BasicCat cat){
        if (catsList[0] == null){
            catsList[0] = cat;
        } else {
            catsReserve = new BasicCat[catsList.length];
            for (int i = 0; i < catsList.length; i++) {
                catsReserve[i] = catsList [i];
            }
            catsList = new BasicCat[catsReserve.length + 1];
            for (int i = 0; i < catsReserve.length; i++) {
                catsList[i] = catsReserve [i];
            }
            catsList[catsList.length - 1] = cat;
        }
    }

    public static void totalCats(){
        System.out.println("There are " + catsList.length + " cats.");
    }

    public static void totalKittens(){
        System.out.println("There are " + numKittens + " kittens.");
    }
}
