package Lesson14;

public class Main {
    final static int MULTIPLIER = 7;
    public static void main(String[] args){

        BasicCat tima = new BasicCat("Tima", 10);
        BasicCat murka = new BasicCat("Murka", 5, 15);
        BasicCat barsik = new BasicCat("Barsik", 0.5, 0.4);
        BasicCat nusha = new BasicCat("Nusha", 0.6, 0.5);

        BasicCat.totalCats();
        BasicCat.totalKittens();
        System.out.println();
        System.out.println("Here they are: ");
        System.out.println();
        for (BasicCat i:BasicCat.catsList) {
            System.out.print("Cat " + i.name + " is " + i.age + " years old.");
            if (i.weight != 0){
                System.out.print(" It's weight is " + i.weight + " kilos.");
            }
            humanAge(i);
        }
    }

    public static void humanAge(BasicCat cat){
        System.out.println("If " + cat.name + " was a human, it's age would be about " + cat.age * MULTIPLIER + " years.");
    }
}
