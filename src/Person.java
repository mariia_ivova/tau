public class Person {
    /*
Реализовать класс, позволяющий хранить информацию о человеке.
Важная информация о человеке - его фамилия, имя, отчество и год рождения. Создайте конструктор.
Подумайте, какие поля стоит сделать немодифицируемыми, какую информацию стоит вынести в конструктор,
какие могут быть конструкторы.
Добавьте в класс Человек из задания ссылку на Адрес из задания 1.
Создайте метод, позволяющий задавать адрес человека и метод, позволяющий получать адрес.
*/
    String firstname, lastname, fathername;
    int birhtyear;
    private Address home;

    Person(String firstnamename, String lastname){
        this.firstname = firstnamename;
        this.lastname = lastname;
    }

    Person (String firstname, String lastname, String fathername, final int birhtyear) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.fathername = fathername;
        this.birhtyear = birhtyear;
    }

    public Address getHome () {
        return home;
    }

    public void setHome (Address home) {
        this.home = home;
    }

}
