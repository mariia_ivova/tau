public class Test_Adress {

    public static void main(String[] args) {
        Address myHome = new Address("Rus","SainP", "Komendantslij", 1, 1,123);
        Address MoomiHome = new Address ("Fin", "Helsinki", "Moomistr");
        System.out.println(myHome.country+" "+myHome.city + " " + myHome.street+" "+myHome.homenumber+" "+myHome.flat+" "+myHome.index);
        System.out.println(MoomiHome.country+" "+MoomiHome.city+" "+MoomiHome.street);

        //2nd task

        Address ivanHome = new Address("Russia", "Moscow", "Leningradskoe shosse", 10, 24, 123);

        Person ivan = new Person ("Ivan", "Ivanov", "Ivanovich", 1950);
        ivan.setHome(ivanHome);

        System.out.println("Second task");
        System.out.println("" + ivan.firstname + " " + ivan.fathername + " " + ivan.lastname + " " + ivan.birhtyear);
        System.out.println(""+ivan.getHome().country+ " " + ivan.getHome().city+" "+ivan.getHome().street+" "+ivan.getHome().homenumber+" "+ ivan.getHome().flat);
    }
}
